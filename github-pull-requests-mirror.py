#! /usr/bin/env python

import argparse
import configparser
import github
import gitlab
import yaml
import sys
import os

from urllib.request import urlopen, Request
import simplejson as json
import string

from optparse import OptionParser

usage = "usage: %prog [options]"
parser = OptionParser(usage)

parser.add_option("-v", "--verbose",
                  action="store_true", dest="verbose", default=False,
                  help="Turn on debugging output")

(options, args) = parser.parse_args()


config_file = '/home/admin/secret/close_pull_requests.cfg'
projects_yaml = '/tmp/repositories.yaml'
github_url = 'https://api.github.com/orgs/GNOME/repos?per_page=100'
excludes_file = '/opt/app-root/src/github.excludes'
github_organization = 'gnome'
repositories = []

if not os.path.isfile(excludes_file):
    print('No excludes file could be found at %s' % excludes_file)
    sys.exit(1)
else:
    exec(open(excludes_file).read())

if not os.path.isfile(config_file):
    GITLAB_RW_TOKEN = os.environ.get('GITLAB_RW_TOKEN')
    GITHUB_OAUTH_TOKEN = os.environ.get('GITHUB_OAUTH_TOKEN')

    secure_config = None
else:
    GITHUB_SECURE_CONFIG = os.environ.get('GITHUB_SECURE_CONFIG',
                                          '%s' % config_file)

    secure_config = configparser.ConfigParser()
    secure_config.read(GITHUB_SECURE_CONFIG)

MESSAGE = """Thank you for contributing to %(project)s!

%(project)s uses GitLab for code review. As such we have forwarded
your contribution to the GNOME official GitLab instance:

%(gitlab_issue_url)s

GNOME's GitLab instance also supports GitHub as one of the possible
authentication method in case you'd like to follow up on the above
issue. You can login at the following URL:

https://gitlab.gnome.org/users/sign_in

If you are interested in sticking around within the GNOME community we
encourage you to read the following documents:

https://www.gnome.org/get-involved
and
https://wiki.gnome.org/Newcomers
"""

TEMPLATE = string.Template("""
- project: $project_name
  options:
    - $has_pull_requests
""")

ISSUE_TEMPLATE = string.Template("""
The following Merge Request (MR) has been forwarded from GitHub in order to prevent
the GNOME Project from losing contributions coming from un-official channels. And for
contributors to not see their valuable contributions not being accounted for.

Relevant information:

Github handle: $user

MR URL: $mr_url

Patch URL: $patch_url

Body of the MR:

$body
""")

def fetch_repositories(url):
    url = str(url)

    if secure_config:
        if secure_config.has_option("github", "oauth_token"):
            auth_token = secure_config.get("github", "oauth_token")
    else:
        auth_token = GITHUB_OAUTH_TOKEN

    req = Request(url)
    req.add_header('Authorization', 'token %s' % auth_token)
    response = urlopen(req)
    link = response.info().get('link')
    response.close()

    links = link.split(' ')
    last_page = links[-2].replace('<','').replace('>','').replace(';','').split('?')[1].split('&')[1].split('=')[1]

    for i in range(1, int(last_page) + 1):
        _url = github_url + '&page=%i' % i
        r = Request(_url)
        r.add_header('Authorization', 'token %s' % auth_token)
        resp = urlopen(r)

        content = resp.read()
        parsed_json = json.loads(content)
        resp.close()

        for repository in parsed_json:
            repo_name = repository['name']
            if options.verbose:
                 print('Appending %s to the repositories list' % repo_name)

            repositories.append(repo_name)

    with open('%s' % projects_yaml, 'w') as repo_list:
        for repo in repositories:
            repo = str(repo)

            if repo in excludes:
                has_pull_requests='has-no-pull-requests'
                repo_list.write(TEMPLATE.substitute(project_name = repo, has_pull_requests=has_pull_requests))
            else:
                has_pull_requests='has-pull-requests'
                repo_list.write(TEMPLATE.substitute(project_name = repo, has_pull_requests=has_pull_requests))

def create_gl_issue(repo_name, title, body, patch_url, mr_url, contributor):
    if secure_config:
        if secure_config.has_option("gitlab", "rw_token"):
            GITLAB_PRIVATE_RW_TOKEN = secure_config.get("gitlab", "rw_token")
    else:
        GITLAB_PRIVATE_RW_TOKEN = GITLAB_RW_TOKEN

    gl = gitlab.Gitlab('https://gitlab.gnome.org', GITLAB_PRIVATE_RW_TOKEN, api_version=4)
    print(repo_name)
    try:
        project = gl.projects.get('GNOME/%s' % repo_name)
    except gitlab.exceptions.GitlabGetError:
        project = gl.projects.get('Infrastructure/%s' % repo_name)

    vars = dict(user=contributor, patch_url=patch_url, body=body, mr_url=mr_url)
    desc = ISSUE_TEMPLATE.substitute(vars)

    issue = project.issues.create({'title': title,
                                   'description': desc, 'labels': ['github'] })

    return issue

def close_pull_requests():

    pull_request_text = MESSAGE

    if secure_config:
        if secure_config.has_option("github", "oauth_token"):
            auth_token = secure_config.get("github", "oauth_token")
            ghub = github.Github(auth_token)
    else:
        ghub = github.Github(GITHUB_OAUTH_TOKEN)

    org = ghub.get_organization(github_organization)

    with open('%s' % projects_yaml, 'r') as yaml_file:
        for section in yaml.safe_load(yaml_file):
            project = section['project']

            if 'options' in section and section['options'][0] == 'has-no-pull-requests':
                if options.verbose:
                    print('EXCLUDES: Project %s has been excluded' % project)

            # Make sure we're supposed to close pull requests for this project
            if 'options' in section and section['options'][0] == 'has-pull-requests':
                repo = org.get_repo(project)

                # Close each pull request
                pull_requests = repo.get_pulls("open")
                for req in pull_requests:
                    title = req._rawData['title']
                    body = req._rawData['body']
                    patch_url = req._rawData['patch_url']
                    contributor = req._rawData['user']['login']
                    mr_url = req._rawData['html_url']

                    issue_data = {"url": repo.url + "/issues/" + str(req.number)}
                    issue = github.Issue.Issue(requester=req._requester,
                                               headers={},
                                               attributes=issue_data,
                                               completed=True)

                    gl_issue = create_gl_issue(project, title, body, patch_url, mr_url, contributor)

                    vars = dict(project=project, gitlab_issue_url=gl_issue.web_url)
                    issue.create_comment(pull_request_text % vars)
                    req.edit(state="closed")

if __name__ == "__main__":
    fetch_repositories(github_url)

    close_pull_requests()
