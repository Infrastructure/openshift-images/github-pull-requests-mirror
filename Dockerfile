FROM registry.access.redhat.com/ubi8/python-38 

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY github-pull-requests-mirror.py /usr/local/bin

ENTRYPOINT ["/usr/local/bin/github-pull-requests-mirror.py"]
